package book

import "time"

type Book struct {
	BookId      int
	Title       string
	Description string
	Price       int
	Rating      int
	Author      string
	CreatedBy   string
	CreatedAt   time.Time
	UpdatedBy   string
	UpdatedAt   time.Time
}
