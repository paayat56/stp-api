package book

type Service interface {
	SelectAll() ([]Book, error)
	SelectSatu(book_id int) (Book, error)
	Create(bookRequest BookRequest) (Book, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) SelectAll() ([]Book, error) {
	books, err := s.repository.SelectAll()
	return books, err
	// return s.repository.SelectAll()
}

func (s *service) SelectSatu(book_id int) (Book, error) {
	book, err := s.repository.SelectSatu(book_id)
	return book, err
	// return s.repository.SelectSatu(book_id)
}

func (s *service) Create(bookRequest BookRequest) (Book, error) {
	book := Book{
		Title:       bookRequest.Title,
		Price:       bookRequest.Price,
		Description: bookRequest.Description,
		Author:      bookRequest.Author,
		Rating:      bookRequest.Rating,
		CreatedBy:   bookRequest.CreatedBy,
		UpdatedBy:   bookRequest.UpdatedBy,
	}

	newBook, err := s.repository.Create(book)
	return newBook, err
	// return s.repository.SelectSatu(book_id)
}
