package book

import "gorm.io/gorm"

type Repository interface {
	SelectAll() ([]Book, error)
	SelectSatu(book_id int) (Book, error)
	Create(book Book) (Book, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) SelectAll() ([]Book, error) {
	var books []Book
	err := r.db.Find(&books).Error
	return books, err
}

func (r *repository) SelectSatu(book_id int) (Book, error) {
	var book Book
	err := r.db.Find(&book, book_id).Error
	return book, err
}

func (r *repository) Create(book Book) (Book, error) {
	err := r.db.Create(&book).Error
	return book, err
}
