package handler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"

	"stp-api/book"
)

type bookHandler struct {
	bookService book.Service
}

func NewBookHandler(bookService book.Service) *bookHandler {
	return &bookHandler{bookService}
}

func (h *bookHandler) RootHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"name": "Daydara",
		"bio":  "Software Engineer",
	})
}

func (h *bookHandler) HelloHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"title":    "Hello World",
		"subtitle": "Welcome to hello wolrd",
	})
}

func (h *bookHandler) BooksHandler(c *gin.Context) {
	// :8089/books/10
	id := c.Param("id")
	// title := c.Param("title")
	c.JSON(http.StatusOK, gin.H{
		"id": id,
		// "title": title,
	})
}

func (h *bookHandler) QueryHandler(c *gin.Context) {
	// :8089/query/?title=bumi-manusia
	title := c.Query("title")
	c.JSON(http.StatusOK, gin.H{
		"title": title,
	})
}

func (h *bookHandler) HandlerPostBooks(c *gin.Context) {
	// title, price
	var bookRequest book.BookRequest

	err := c.ShouldBindJSON(&bookRequest)
	if err != nil {

		errorMessages := []string{}

		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on filed %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		// c.JSON(http.StatusBadRequest, err)
		c.JSON(http.StatusBadRequest, gin.H{
			"err": errorMessages,
		})

		return
	}

	book, err := h.bookService.Create(bookRequest)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})

		return

	}

	c.JSON(http.StatusOK, gin.H{
		"Data": book,
	})
}
